import numpy as np

from .pulse import Pulse


class BurpPulse(Pulse):
    """Generic burp pulse class."""
    def pulse_fun(self, t):
        w = 2.0*np.pi/self.length
        x = t - self.timing - self.length/2.0
        res = 0*t + self.an[0]
        for n, (an, bn) in enumerate(zip(self.an[1:], self.bn)):
            res += (an*np.cos((n+1)*w*x)
                    + bn*np.sin((n+1)*w*x))
        return res/self.c


class Eburp2Pulse(BurpPulse):
    an = [+0.26,
          +0.91, +0.29, -1.28, -0.05, +0.04, +0.02, +0.06, +0.00, -0.02, +0.00]
    bn = [-0.16, -1.82, +0.18, +0.42, +0.07, +0.07, -0.01, -0.04, +0.00, +0.00]
    c = 4.2601187662429085


class Iburp2Pulse(BurpPulse):
    an = [+0.50,
          +0.81, +0.07, -1.25, -0.24, +0.07, +0.11, +0.05, -0.02, -0.03, -0.02, +0.00]
    bn = [-0.68, -1.38, +0.20, +0.45, +0.23, +0.05, -0.04, -0.04, +0.00, +0.01, +0.01]
    c = 4.96826209788


class UburpPulse(BurpPulse):
    an = [+0.27,
          -1.42, -0.37, -1.84, +4.40, -1.19, +0.00, -0.37, +0.50, -0.31, +0.18,
          -0.21, +0.23, -0.12, +0.07, -0.06, +0.06, -0.04, +0.03, -0.02, +0.02]
    bn = [0]*len(an)
    c = 10.9681816143


class ReburpPulse(BurpPulse):
    an = [+0.49,
          -1.02, +1.11, -1.57, +0.83, -0.42, +0.26, -0.16, +0.10, -0.07, +0.04,
          -0.03, +0.01, -0.02, 0.00, -0.01]
    bn = [0]*len(an)
    c = 6.13956814873


class CustomBurp(BurpPulse):
    def __init__(self, an, bn, *args, **kwargs):
        if len(an) != len(bn) + 1:
            raise ValueError('Size mismatch in an, bn coefficients.')
        self.an = an
        self.bn = bn
        self.c = 1.0
        BurpPulse.__init__(self, *args, **kwargs)
