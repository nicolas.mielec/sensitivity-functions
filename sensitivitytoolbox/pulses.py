"""Pulses implementation.

Nicolas Mielec
nicolas.mielec@gmail.com
02/05/2017
"""
import numpy as np
from .pulse import Pulse


class SquarePulse(Pulse):
    """Simple Square Pulse."""
    def pulse_fun(self, t):
        return 1.0


class GaussianPulse(Pulse):
    """Gausian Pulse.

    Additionnal parameter sigma to use in place of length and how many sigmas
    before it should be cut.
    """

    def __init__(self, n=6, order=1.0, sigma=None, *args, **kwargs):
        self.n = n
        self.order = order

        if sigma is not None:
            length = 2 * self.n * sigma
            Pulse.__init__(self, length=length, *args, **kwargs)
        else:
            Pulse.__init__(self, *args, **kwargs)

    def pulse_fun(self, t):
        x = (t - self.timing) * self.n / self.length
        return np.exp(-2.0 * x ** (2 * self.order))

    @property
    def sigma(self):
        return self.length / (2.0 * self.n)

    @sigma.setter
    def sigma(self, sigma):
        self.length = sigma * 2.0 * self.n

    @property
    def proper_length(self):
        return 2 * self.sigma


class HanningPulse(Pulse):
    """Hanning pulse."""
    def pulse_fun(self, t):
        return 0.5 + 0.5 * np.cos(2.0 * np.pi * (t - self.timing) / self.length)


class SincPulse(Pulse):
    """Sinc pulse."""
    def __init__(self, n=5.0, *args, **kwargs):
        self.n = n
        Pulse.__init__(self, *args, **kwargs)

    def pulse_fun(self, t):
        x = (t - self.timing) / self.length * 2.0 * np.pi * self.n
        tol = 1e-7
        try:
            x[abs(x) < tol] = tol
        except TypeError:
            if abs(x) < tol:
                x = tol
        return np.sin(x) / x


class GSincPulse(Pulse):
    """Sinc pulse with gaussian modulation.

    n is number of zero crossing from the center.
    r is gaussian sigma in units of pulse total length
    """
    def __init__(self, n=5.0, r=0.2, order=1.0, *args, **kwargs):
        self.n = n
        self.r = r
        self.order = order
        Pulse.__init__(self, *args, **kwargs)

    def pulse_fun(self, t):
        x = (t - self.timing) / self.length * 2.0 * np.pi * self.n
        u = (t - self.timing) / (self.r * self.length)
        tol = 1e-7
        try:
            x[abs(x) < tol] = tol
        except TypeError:
            if abs(x) < tol:
                x = tol
        return np.sin(x) / x * np.exp(-0.5 * u ** (2 * self.order))


class HalfGaussianPulse(Pulse):
    def __init__(self, reverse=False, n=6,
                 *args, **kwargs):
        self.n = n
        self.reverse = reverse
        Pulse.__init__(self, *args, **kwargs)

    def pulse_fun(self, t):
        if self.reverse:
            x = (t - self.timing + self.length / 2.0) * self.n / self.length
        else:
            x = (t - self.timing - self.length / 2.0) * self.n / self.length
        return np.exp(-2.0 * x ** 2.0)

    @property
    def proper_length(self):
        return self.length / self.n


class CavityPulse(Pulse):
    def __init__(self, tau, n=10.0, pulse_area=np.pi / 2, *args, **kwargs):
        self.n, self.tau = n, tau

        rabi_max = kwargs.pop('rabi_max', None)
        length = kwargs.pop('length', None)
        if rabi_max is None and length is None:
            super(CavityPulse, self).__init__(rabi_max=rabi_max,
                                              length=length,
                                              *args, **kwargs)
        elif rabi_max is None:
            if length < n * tau:
                message = u'`length` must be larger than `n` x `tau`'
                raise ValueError(message)
            T = length - n * tau
            rabi_max = pulse_area / T
            super(CavityPulse, self).__init__(rabi_max=rabi_max,
                                              length=length,
                                              *args, **kwargs)
        else:
            T = pulse_area / rabi_max
            length = T + n * tau
            super(CavityPulse, self).__init__(rabi_max=rabi_max,
                                              length=length,
                                              *args, **kwargs)

    def pulse_fun(self, t):
        L, tau, n = self.length, self.tau, self.n
        tf = self.timing + L / 2.0 - n*tau
        dt = L - n * tau
        u = (t - self.timing + L / 2.0)/tau
        v = (t - tf) / tau
        out = 0 * t
        # out = 1 - np.exp(-u)
        mask = (0 < u) & (u < dt / tau)
        try:
            out[mask] = 1 - np.exp(-u[mask])
            out[v > 0] = (1 - np.exp(-dt / tau)) * np.exp(-v[v > 0])
        except TypeError:
            if mask:
                out = 1 - np.exp(-u)
            if v > 0:
                out = (1 - np.exp(-dt / tau)) * np.exp(-v)
        return out

    @property
    def proper_length(self):
        return self.length - (self.n - 1) * self.tau


class HalfGaussianFlatPulse(Pulse):
    def __init__(self, reverse=False, n=6.0, r=1.0,
                 *args, **kwargs):
        self.n = n
        self.r = r
        self.reverse = reverse
        Pulse.__init__(self, *args, **kwargs)

    def pulse_fun(self, t):
        r, n, L = self.r, self.n, self.length
        if self.reverse:
            tg = self.timing - L * (0.5 - 1.0 / (1 + n * r))
            u = -(t - tg) * (1 + n * r) / (r * L)
        else:
            tg = self.timing + L * (0.5 - 1.0 / (1 + n * r))
            u = (t - tg) * (1 + n * r) / (r * L)
        out = np.exp(-2.0 * u ** 2.0)
        try:
            out[u > 0] = 1.0
        except TypeError:
            if u > 0:
                out = 1.0
        return out

    @property
    def proper_length(self):
        return (1 + self.r) * self.length / (1 + self.n * self.r)


class GFlatPulse(Pulse):
    """Gaussian Flat Pulse
    n is the number of sigma of the gaussian decay after which the pulse is cut
    r is the ratio of the gaussian sigma to the plateau length
    """
    def __init__(self, n=6.0, r=0.5, *args, **kwargs):
        self.n, self.r = n, r
        super(GFlatPulse, self).__init__(*args, **kwargs)

    def pulse_fun(self, t):
        r, n, L = self.r, self.n, self.length
        s = r * L / (1.0 + 2.0 * n * r)
        ul = (t - self.timing + 0.5 * L / (1.0 + 2.0 * n * r)) / s
        ur = (t - self.timing - 0.5 * L / (1.0 + 2.0 * n * r)) / s
        out = t * 0 + 1
        try:
            out[ul < 0] = np.exp(-2.0 * ul[ul < 0]**2)
            out[ur > 0] = np.exp(-2.0 * ur[ur > 0]**2)
        except TypeError:
            if ul < 0:
                out = np.exp(-2.0 * ul**2)
            elif ur > 0:
                out = np.exp(-2.0 * ur**2)
        return out

    @property
    def proper_length(self):
        return (1 + 2 * self.r) * self.length / (1 + 2 * self.n * self.r)


class FermiDiracPulse(Pulse):
    def __init__(self, n=6.0, beta=16.8, *args, **kwargs):
        self.n, self.beta = n, beta
        super(FermiDiracPulse, self).__init__(*args, **kwargs)

    def pulse_fun(self, t):
        b, n, L = self.beta, self.n, self.length
        # 1.0/(1.0 + np.exp(b*(t/R - 1.0)))
        u = abs(t - self.timing) / (L / (2.0 * n))
        out = 0 * t
        try:
            out[u < n] = 1.0 / (1.0 + np.exp(b * (u[u < n] - 1.0)))
        except TypeError:
            if u < n:
                out = 1.0 / (1.0 + np.exp(b * (u - 1.0)))
        return out

    @property
    def proper_length(self):
        return 2 * self.length / (2 * self.n)

