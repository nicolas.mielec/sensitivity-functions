from math import pi

from .interferometer import Interferometer
from .pulses import SquarePulse


class OnePulseInterferometer(Interferometer):
    def __init__(self, length_pi=None, rabi_max=0,
                 pulse_class=SquarePulse, pulse_kwargs=dict()):
        if length_pi is None and rabi_max is None:
            raise ValueError('Either tau_pi or rabi_max must be provided')

        if length_pi is None:
            pulses = [pulse_class(rabi_max=rabi_max, timing=0,
                                  pulse_area=pi, **pulse_kwargs)]
        else:
            pulses = [pulse_class(length=length_pi, timing=0,
                                  pulse_area=pi, **pulse_kwargs)]
        Interferometer.__init__(self, pulses)


class Gyroscope(Interferometer):
    def __init__(self, length_pi=None, rabi_max=None, T=0,
                 pulses_cfg=[(SquarePulse, dict())]):

        if len(pulses_cfg) == 1:
            pulses_cfg *= 4
        if length_pi is None and rabi_max is None:
            pass
        if length_pi is not None:
            pulses_cfg[0][1]['length'] = length_pi / 2
            pulses_cfg[1][1]['length'] = length_pi
            pulses_cfg[2][1]['length'] = length_pi
            pulses_cfg[3][1]['length'] = length_pi / 2
        elif rabi_max is not None:
            for _, pulse_kwargs in pulses_cfg:
                pulse_kwargs['rabi_max'] = rabi_max

        timings = [0, T, 3 * T, 4 * T]
        pulse_areas = [pi / 2, pi, pi, pi / 2]
        pulses = [pulse_cfg[0](timing=timing, pulse_area=pulse_area,
                               **pulse_cfg[1])
                  for timing, pulse_area, pulse_cfg
                  in zip(timings, pulse_areas, pulses_cfg)]

        Interferometer.__init__(self, pulses)

    def append(self, pulse):
        raise NotImplemented

    @property
    def first_pulse(self):
        return self[0]

    @property
    def second_pulse(self):
        return self[1]

    @property
    def third_pulse(self):
        return self[2]

    @property
    def fourth_pulse(self):
        return self[3]

    def set_T(self, T):
        self.timings = [0, T, 3 * T, 4 * T]
    T = property(fset=set_T)


class Clock(Interferometer):
    def __init__(self, length_pi=None, rabi_max=None, T=0,
                 pulses_cfg=[(SquarePulse, dict())]):

        if len(pulses_cfg) == 1:
            pulses_cfg *= 2
        if length_pi is None and rabi_max is None:
            pass
        if length_pi is not None:
            for _, pulse_kwargs in pulses_cfg:
                pulse_kwargs['length'] = length_pi / 2
        elif rabi_max is not None:
            for _, pulse_kwargs in pulses_cfg:
                pulse_kwargs['rabi_max'] = rabi_max

        pulses = [pulses_cfg[0][0](timing=0, pulse_area=pi / 2,
                                   **pulses_cfg[0][1]),
                  pulses_cfg[1][0](timing=T, pulse_area=pi / 2,
                                   **pulses_cfg[1][1])]

        Interferometer.__init__(self, pulses)

    def append(self, pulse):
        raise NotImplemented

    @property
    def first_pulse(self):
        return self[0]

    @property
    def second_pulse(self):
        return self[1]

    def set_T(self, T):
        self.timings = [0, T]
    T = property(fset=set_T)


class Gravimeter(Interferometer):
    def __init__(self, length_pi=None, rabi_max=None, T=0,
                 pulses_cfg=[(SquarePulse, dict())]):

        timings = [0, T, 2 * T]
        pulse_areas = [pi / 2, pi, pi / 2]

        if len(pulses_cfg) == 1:
            pulses_cfg *= 3
        if length_pi is None and rabi_max is None:
            pass
        if length_pi is not None:
            pulses_cfg[0][1]['length'] = length_pi / 2
            pulses_cfg[1][1]['length'] = length_pi
            pulses_cfg[2][1]['length'] = length_pi / 2
        elif rabi_max is not None:
            for _, pulse_kwargs in pulses_cfg:
                pulse_kwargs['rabi_max'] = rabi_max

        pulses = [pulse_cfg[0](timing=timing, pulse_area=pulse_area,
                               **pulse_cfg[1])
                  for timing, pulse_area, pulse_cfg
                  in zip(timings, pulse_areas, pulses_cfg)]

        Interferometer.__init__(self, pulses)

    def append(self, pulse):
        raise NotImplemented

    @property
    def first_pulse(self):
        return self[0]

    @property
    def second_pulse(self):
        return self[1]

    @property
    def third_pulse(self):
        return self[2]

    @property
    def fourth_pulse(self):
        return self[3]

    def set_T(self, T):
        self.timings = [0, T, 3 * T]
    T = property(fset=set_T)
