import scipy.interpolate as si
import numpy as np

from .pulse import Pulse


class InterpolatedPulse(Pulse):
    def __init__(self, values, *args, **kwargs):
        self.n_samples = len(values)
        self.interp_values = si.interp1d(
            x=np.arange(self.n_samples).astype(float),
            y=values / values.max(),
            kind='cubic',
            bounds_error=False, fill_value=0,
        )

        super(InterpolatedPulse, self).__init__(*args, **kwargs)

    def pulse_fun(self, t):
        corrected_t = (t - (self.timing - 0.5*self.length)) / self.length * self.n_samples
        return self.interp_values(corrected_t)