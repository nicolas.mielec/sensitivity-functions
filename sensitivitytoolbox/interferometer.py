import numpy as np
import scipy.integrate as si
import scipy.fftpack as sf
import scipy.signal as ss
import matplotlib.pyplot as plt
from math import pi, log, ceil

from .pulse import Pulse
from .functions import stride_mean


class Interferometer(list):
    """
    This class provides simple methods to get the sensitivity function,
    transfer function and scale factor of an interferometer with arbitrary
    temporally shaped pulses amplitudes.

    It is initialized with a list of pulses, and pulses can be appended to it.
    """

    def append(self, pulse):
        if not isinstance(pulse, Pulse):
            raise ValueError('Interferometer append expects a Pulse')
        list.append(Interferometer, pulse)
        self.init_sensitivity()

    def __init__(self, pulse_list=None):
        if pulse_list is not None:
            for pulse in pulse_list:
                if not isinstance(pulse, Pulse):
                    message = ('Interferometer initialisation '
                               'expects list of Pulses')
                    raise ValueError(message)
            list.__init__(self, pulse_list)
            self.init_sensitivity()

    @property
    def begin(self):
        return min([pulse.begin for pulse in self])

    @property
    def end(self):
        return max([pulse.end for pulse in self])

    @property
    def length(self):
        return self.end - self.begin

    def __str__(self):
        out = "Interferometer :"
        for pulse in self:
            out += " - " + pulse.__str__()
        return out

    def init_rabi(self):
        """
        This method defines the rabi amplitude function of the interferometer
        as the sum of all the pulses amplitudes at a time t.
        It should be called whenever the interferometer pulses are modified.
        """
        def rabi_fun(t):
            result = 0.0 * t
            for pulse in self:
                result += pulse(t)
            return result
        self.rabi_fun = rabi_fun

    def init_sensitivity(self, tol=1e-10, mxstep=10000):
        """
        This method defines the sensitivity function of the interferometer.
        It is the sinus of the integral of the rabi amplitude.
        """
        self.init_rabi()

        hmax = min([(pulse.end - pulse.begin) / 2.0 for pulse in self])
        ode_params = dict(rtol=tol, atol=tol, hmax=hmax, mxstep=mxstep)

        def F(times):
            times = np.array([times]).flatten()
            if times[0] > self.begin:
                times = np.insert(times, 0, self.begin)
                res = si.odeint(lambda y, t: self.rabi_fun(t),
                                0, times, **ode_params)
                res = np.sin(res[1:])
            else:
                res = np.sin(si.odeint(lambda y, t: self.rabi_fun(t),
                                       0, times, **ode_params))
            return res.flatten()

        self.sensitivity_fun = F

    def scale_factor(self, tol=1e-11):
        """
        Calculates the scale factor of the interferometer
        """
        self.init_rabi()

        def sys(t, y):
            return [self.rabi_fun(t), t * np.sin(y[0])]

        r = si.ode(sys).set_integrator('dopri5', rtol=tol)
        r.set_initial_value([0, 0], self.begin)
        for pulse in self:
            r.integrate(pulse.center)
        return abs(r.integrate(self.end)[1])

    def transfer_function(self, F_max=1e7, df=1,
                          average=False, f_cut=None, f_stride=None):
        """
        Calculates the transfer function of the interferometer.

        Lowest frequency df is bounded by the inverse of interferometer length
        The total number of samples is then adjusted
        """

        self.init_sensitivity()

        t_int = self.end - self.begin
        df_min = 1 / t_int / 5.0
        if df > df_min:
            df = df_min

        T = 1 / df
        N = 2**(int(log(2 * F_max / df) / log(2)) + 1)

        dt = T / N
        n = int(ceil(t_int / dt))
        if N < 2**26:
            t = np.linspace(self.begin, self.end, n)
            dt = t[1] - t[0]
            f = np.linspace(0.0, 1 / (2 * dt), N / 2)
            S = self.sensitivity_fun(t)
            fft_sens = sf.fft(S, N)[:int(N / 2)] * dt
            H = 2 * pi * f * abs(fft_sens)
        else:
            message = ('Too many points to calculate. Consider reducing the '
                       'frequency requirements')
            raise ValueError(message)

        # If the average is asked, take the mean of f_stride chunks of the
        # transfer function after the f_cut frequency
        if average:
            if f_cut is None or f_stride is None:
                raise ValueError('f_cut and f_stride should be provided.')

            df = f[1] - f[0]
            idx = f < f_cut
            f_low = f[idx]
            H_low = H[idx]

            idx_minima = ss.argrelextrema(H_low, np.less)[0]
            H_low[idx_minima] = 1e-20

            idx = f >= f_cut
            f_high = stride_mean(f[idx], int(f_stride / df))
            H_high = stride_mean(H[idx], int(f_stride / df))

            f_high = f_high - f_high[0] + f_low[-1]

            f_out = np.hstack((f_low, f_high))
            H_out = np.hstack((H_low, H_high))
            return f_out, H_out

        return (f, H)

    @property
    def timings(self):
        return [pulse.timing for pulse in self]

    @timings.setter
    def timings(self, timings):
        if len(timings) != len(self):
            raise ValueError('Timings length should match the number of '
                             'pulses')
        for pulse, timing in zip(self, timings):
            pulse.timing = timing

    def plot_sensitivity(self, ax=plt, N=1000):
        t = np.linspace(self.begin, self.end, N)
        ax.plot(t, self.sensitivity_fun(t))

    def plot_rabi(self, ax=plt, N=1000):
        t = np.linspace(self.begin, self.end, N)
        ax.plot(t, self.rabi_fun(t))
