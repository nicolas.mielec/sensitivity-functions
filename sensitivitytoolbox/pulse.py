"""Pulse class.

Nicolas Mielec
nicolas.mielec@gmail.com
02/05/2017
"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as si

"""
Template Pulse subclass

class TemplatePulse(Pulse):

    def __init__(self, other_args, *args, **kwargs):
        # Should not be defined if no other_args defined
        # See CavityPulse for a bit more complex definition

        super(TemplatePulse, self).__init__(*args, **kwargs)

    def pulse_fun(self, t):
        # temporal pulse shape normalized to 1
        return ...

"""

class Pulse(object):
    """Generic pulse class.

    Defines a template class for actual pulses
    initialisable with pulse `length`, maximum pulsation `rabi_max`,
    `timing` and `pulse_area`
    When called with time parameter, it returns the pulse value at that time

    Subclasses should define the `pulse_fun` method : the actual pulse shape
    normalized to 1

    See Pulses.py for example implementations and below for template
    """
    def __init__(self, rabi_max=None, length=None,
                 pulse_area=np.pi / 2, timing=0, phase=0):
        self.rabi_max = 1.0
        self.timing = timing
        self.length = 1.0
        self.pulse_area = pulse_area
        self.phase = phase

        if rabi_max is None and length is None:
            message = 'Either `rabi_max` or `length` have to be provided'
            raise ValueError(message)

        if rabi_max is not None and length is not None:
            self.rabi_max = rabi_max
            self.length = length
            self.pulse_area = si.quad(lambda t: self(t),
                                      self.begin, self.end,
                                      limit=10000)[0]
        else:  # adjust rabi_max or length to have the right area
            if rabi_max is None:
                self.length = length
                self.rabi_max = 1.0 / length
                self.rabi_max *= pulse_area / si.quad(lambda t: self(t),
                                                      self.begin, self.end,
                                                      limit=10000)[0]
            else:
                self.rabi_max = rabi_max
                self.length = 1.0 / rabi_max
                area = si.quad(lambda t: self(t),
                               self.begin, self.end,
                               limit=10000)[0]
                self.length *= pulse_area / area

    def __repr__(self):
        template = ('{name}(rabi_max={rabi_max:.2e}, length={length:.2e}, '
                    'timing={timing:.2e}, area=')
        n_pi = self.pulse_area / np.pi
        if n_pi > 1:
            template += '{n_pi:.1f}*pi)'
        else:
            template += 'pi/{n_pi:.0f})'
            n_pi = 1 / n_pi
        return template.format(name=self.__class__.__name__,
                               rabi_max=self.rabi_max,
                               length=self.length,
                               timing=self.timing,
                               n_pi=n_pi)

    def __len__(self):
        return self.length

    @property
    def center(self):
        return 0.5 * (self.end + self.begin)

    @property
    def begin(self):
        return self.timing - self.length / 2.0

    @property
    def end(self):
        return self.timing + self.length / 2.0

    @property
    def proper_length(self):
        return self.length

    # rabi_frequency x f(t)  in [begin, end]
    def __call__(self, t):
        return (self.rabi_max * self.pulse_fun(t) *
                ((self.begin < t) & (t < self.end)))

    def plot(self, num=50, ax=None, normalize=True, **kwargs):
        t = np.linspace(self.begin - 0.1 * self.length,
                        self.end + 0.1 * self.length, num=num)
        line = np.real(self(t))
        norm = 1.0
        if ax is None:
            _, ax = plt.subplots()
        if normalize:
            norm = max(line)
        ax.plot(t, line / norm, **kwargs)


