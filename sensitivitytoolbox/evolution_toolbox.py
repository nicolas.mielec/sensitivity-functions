import numpy as np

from . import odeintw


def evolution_system(y, t, pulse, phase=lambda t: 0, detuning=lambda t: 0):
    return [-1j*0.5*pulse(t)*np.exp(+1j*(phase(t) + detuning(t)*t))*y[1],
            -1j*0.5*np.conj(pulse(t))*np.exp(-1j*(phase(t) + detuning(t)*t))*y[0]]


def pulse_selectivity(pulse, detunings, start_state=[1.0+0.0j, 0.0j],
                      tol=1e-5):
    """ calculates the pulse velocity selectivity """

    Y = []
    for detuning in detunings:
        states = odeintw.odeintw(evolution_system,
                                 start_state, [pulse.begin, pulse.end],
                                 args=(pulse, lambda t: 0, lambda t: detuning),
                                 rtol=tol, atol=tol, mxstep=int(1e6),
                                 hmax=(pulse.end-pulse.begin)/100)
        Y.append(abs(states[-1, 1])**2)

    return Y


def state_evolution(pulse, start_state=[1.0+0.0j, 0.0j], num=150, tol=1e-5):
    """ calculates state evolution for a given pulse """

    times = np.linspace(pulse.begin, pulse.end, num)
    states = odeintw.odeintw(evolution_system,
                             start_state, times,
                             args=(pulse, lambda t: 0, lambda t: 0),
                             rtol=tol, atol=tol, mxstep=int(1e6),
                             hmax=(pulse.end-pulse.begin)/100)
    Y = abs(states[:, 1])**2
    return times, Y
